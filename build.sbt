name := "kerberos"

organization := "com.oidev.bridgegate"

version := "0.1.0-SNAPSHOT"

scalaVersion := "2.9.1"

libraryDependencies := Seq(
  "org.scalaz" %% "scalaz-core" % "6.0.3",
  "commons-codec" % "commons-codec" % "1.4",
  "org.specs2" %% "specs2" % "1.6.1" % "test",
  "org.specs2" %% "specs2-scalaz-core" % "6.0.1" % "test"
)

initialCommands := "import com.oidev.bridgegate.kerberos._"
