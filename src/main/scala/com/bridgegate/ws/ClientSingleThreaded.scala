/**
 * <p>
 * Copyright (c) 2001, 2010, Object Innovation, Inc. All Rights Reserved.
 *
 * This software is published under the terms of the Object Innovation
 * License version 1.1, a copy of which has been included with this
 * distribution in the LICENSE.TXT file. Learn more at http://www.bridgegatetei.com
 * </p>
 */
package com.bridgegate.ws

import java.io.IOException
import java.net.Authenticator
import java.net.HttpURLConnection
import java.net.PasswordAuthentication
import javax.security.auth.login.Configuration
import java.net.URL

import com.bridgegate.ws.Base._
import scala.collection.JavaConverters._

/**
 * @author bcarlson
 * @date Jul 26, 2011
 */
object ClientSingleThreaded extends App {
  val (Options(username, password, realm, targetUrl)) = parseArgs(args) 

  init(username, password)
  accessURL(targetUrl) ~ println

  def init(user: String, pwd: String) = {
    System.setProperty("java.security.krb5.conf", "./krb.conf")
    System.setProperty("java.security.auth.login.config", "./jaas.conf")

    class Auth(val username: String, val password: String) extends Authenticator {
      override def getPasswordAuthentication() = {
        println("Getting username and password")
        new PasswordAuthentication(username, password.toCharArray())
      }
    }

    Authenticator.setDefault(new Auth(user, pwd))
  }

  def accessURL(targetUrl: URL): String = {
    println("Accessing: %s".format(targetUrl))
    receive(targetUrl.openConnection().asInstanceOf[HttpURLConnection] ~ { con =>
      con.setRequestMethod("GET")
    })
  }
}
