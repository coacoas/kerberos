/**
 * <p>
 * Copyright (c) 2001, 2010, Object Innovation, Inc. All Rights Reserved.
 *
 * This software is published under the terms of the Object Innovation
 * License version 1.1, a copy of which has been included with this
 * distribution in the LICENSE.TXT file. Learn more at http://www.bridgegatetei.com
 * </p>
 */
package com.bridgegate.ws

import java.io.File
import java.io.FileWriter
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL
import java.security.PrivilegedAction
import org.apache.commons.codec.binary.Base64
import org.ietf.jgss.GSSContext
import org.ietf.jgss.GSSCredential
import org.ietf.jgss.GSSManager
import org.ietf.jgss.GSSName
import org.ietf.jgss.Oid
import javax.security.auth.login.LoginContext
import javax.security.auth.Subject
import scala.io.Source
import scala.collection.JavaConverters._
import Base._

/**
 * @author bcarlson
 * @date Jul 26, 2011
 */
object ClientThreadSafe extends App {

  def writeToDisk = encodeAndWriteTicketToDisk("./security.token")

  //---------------------------------------------------------

  val (Options(username, password, realm, targetUrl)) = parseArgs(args) 
  init
  val serviceTicket = Some(initiateSecurityContext(username, password, realm))
  accessURL(targetUrl, serviceTicket) ~ println
  
  //---------------------------------------------------------

  def init = {
    import System._
    setProperty("java.security.krb5.conf", "./krb.conf")
    setProperty("java.security.auth.login.config", "./jaas.conf")
  }

  def login(username: String, password: String) = {
    (new LoginContext("kerberos", new LoginCallbackHander(username, password)) ~ { ctx =>
      ctx.login
    }).getSubject
  }

  def initiateSecurityContext(username: String, password: String, realm: String) = {
    val subject = login(username, password)
    val spnegoMechOid = new Oid("1.3.6.1.5.5.2")
    val krb5MechOid = new Oid("1.2.840.113554.1.2.2")
    val manager = GSSManager.getInstance();

    val gssCredential = Subject.doAs(subject,
      new PrivilegedAction[GSSCredential] {
        override def run = {
          import GSSCredential._
          val gssName = manager.createName(username,
            GSSName.NT_USER_NAME,
            krb5MechOid)

          manager.createCredential(
            gssName.canonicalize(krb5MechOid),
            DEFAULT_LIFETIME,
            krb5MechOid,
            INITIATE_ONLY) ~ { cred =>
              import cred._
              add(gssName,
                INDEFINITE_LIFETIME,
                INDEFINITE_LIFETIME,
                spnegoMechOid,
                INITIATE_ONLY);
            }
        }
      })

    val gssServerName = manager.createName("%s@%s".format("HTTP/%s".format(targetUrl.getHost), realm), 
        GSSName.NT_USER_NAME)
    val clientContext = manager.createContext(
      gssServerName.canonicalize(spnegoMechOid),
      spnegoMechOid,
      gssCredential,
      GSSContext.DEFAULT_LIFETIME) ~ { ctx =>
        ctx.requestCredDeleg(true)
      }

    clientContext.initSecContext(Array[Byte](0), 0, 0)
  }

  def accessURL(targetUrl: URL, ticket: Option[Array[Byte]] = None): String = {
    val con = targetUrl.openConnection().asInstanceOf[HttpURLConnection] ~ { c =>
      c.setRequestMethod("GET")

      ticket foreach { tkt =>
        c.setRequestProperty("Authorization",
          "Negotiate %s".format(new Base64(4096).encodeToString(tkt).stripLineEnd))
      }
    }

    receive(con)
  }

  /**
   * Writes the ticket to disk, but in a URL-safe way. This should
   * not be the string sent on the HTTP header.
   *
   * @param ticket
   * @param filepath
   */
  def encodeAndWriteTicketToDisk = (filepath: String) => (ticket: Array[Byte]) => {
    val fw = new FileWriter(new File(filepath))
    fw.write(Base64.encodeBase64URLSafeString(ticket))
    fw.close()
  }
}
