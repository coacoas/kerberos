/**
 * <p>
 * Copyright (c) 2001, 2010, Object Innovation, Inc. All Rights Reserved.
 *
 * This software is published under the terms of the Object Innovation
 * License version 1.1, a copy of which has been included with this
 * distribution in the LICENSE.TXT file. Learn more at http://www.bridgegatetei.com
 * </p>
 */
package com.bridgegate.ws
import javax.security.auth.callback.Callback
import javax.security.auth.callback.CallbackHandler
import javax.security.auth.callback.NameCallback
import javax.security.auth.callback.PasswordCallback

class LoginCallbackHander(val username: String, val password: String) extends CallbackHandler {
  def handle(callbacks: Array[Callback]) {
    callbacks.foreach {
      _ match {
        case nc: NameCallback => nc.setName(username)
        case pw: PasswordCallback => pw.setPassword(password.toCharArray())
        case _ => println("Unknown callback")
      }
    }
  }
}