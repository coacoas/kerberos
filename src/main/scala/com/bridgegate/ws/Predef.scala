package com.bridgegate.ws

import java.net.URLConnection
import scala.collection.JavaConverters._
import io.Source
import java.net.HttpURLConnection
import java.net.URL

case class Options(username: String, password: String, realm: String, url: URL)

object Base {
  def parseArgs(args: Array[String]) = {
    val targetUrl = new URL(args(0))

    val UserRegex = """(\w+)@([\w.]+)""".r
    val (username, realm) = args(1) match {
      case UserRegex(u, r) => (u, r)
      case _               => ("NOUSER", "NO.REALM.SPECIFIED")
    }

    val password = args(2)

    Options(username, password, realm, targetUrl)
  }

  def ??? = sys.error("Not implemented yet")

  /**
   * Allows for chaining with a T => Unit function
   * without having to wrap it in the client code.
   * This was taken (blatantly) from
   * @link{http://hacking-scala.posterous.com/side-effecting-without-braces}
   *
   */
  implicit def anyWithSideEffects[T](any: T) = new {
    def ~(fn: T => Unit): T = {
      fn(any)
      any
    }
  }

  def receive(con: HttpURLConnection): String = try {
    (con.getResponseCode() match {
      case 200 =>
        Source.fromInputStream(con.getInputStream).getLines()
      case x: Int =>
        "Received status code: %d".format(x) +:
          con.getHeaderFields().asScala.foldLeft(Seq[String]()) {
            case (a, entry) => entry match {
              case (k, v) => a :+ "%s => %s".format(k, v)
            }
          }
    }).mkString("\n")
  } catch {
    case e: Exception => e.getLocalizedMessage
  }

}